# SmtpFix-rs ![KPI logo](https://gitlab.com/4neko/smtpfix-rs/-/raw/master/logo_600.png?ref_type=heads&inline=true)

A small and simple synchronous Rust library/crate for mail submission to the server as a client.

### Supports:  
- 8BITMIME only! Does not support 7-bit encoding  
- Authentication: PLAIN, LOGIN, CRAM-MD5, OAUTH2(token), SCRAM-SHA-1/(256/512-PLUS)
- STARTTLS, TLS  
- UTF-8 messages ONLY!
- TLS entity and root certs

### ToDo:
- 7-bit encoding (non UTF-8)
- async networking
- message formatting (attachments)
- Local Smtp Transport (locla storage)

### Usage

```rust
// Create a new Network Transport which will be used to connect with remote server
//NetworkEncType::Starttls force to use Starttls on port 25
//If you want use TLS then set port to 465 and use NetworkEncType::Tls()
let tls = Tls::new(None, TlsProperties::Secure)?;
let transport = NetworkTransport::new("mail.domail.tld", 25, NetworkEncType::starttls_unsafe(tls)); 

// Create a smtp auth container
// if the auth method will be OAUTH2 then instead of password set auth tocken.
let creds = SmtpAuthCreds::new("test@domain.tld", "password123")?;

// Create new instance of the SMTP protocol handler
// The EHLO shoule be set manually.
// Provide the SmtpAuthCreds and transport to the instance
let mut smtp = Smtp::new(
            "ppp-xx-xx-xx-xx.myprovider.tld",
            Some(creds), 
            SmtpAuthType::Secure,  
            transport)?;
```
The code abobve creates instance, it does not perform any attempts to connect.  

```rust
// Creates instance of the Envelope.
let mut envlp = EmsgEnvelopeTemplate::new("test@domain.tld".to_string(), "Test message, smtpfix проверка".to_string(), "domail.tld")?;
envlp.to("user@example.org".to_string())?;

// Another envelope
let mut envlp2 = EmsgEnvelopeTemplate::new("test@domain.tld".to_string(), "Test message, smtpfix проверка 2@#".to_string(), "domail.tld")?;
    envlp2.to("user@example.org".to_string())?;
    envlp2.to("postmaster@foo.bar".to_string())?;

//Join envelpe with email body and push it to the send list
let mut testlist = Vec::<(&EmsgEnvelopeTemplate, String)>::new();
testlist.push((&envlp, "test 123".to_string()));
testlist.push((&envlp2, "Проверка почты".to_string()));

// send
smtp.send(testlist)?;
```

Function send(...) attempts to establish a connection and submit mail. On completion, the connection will be closed.




